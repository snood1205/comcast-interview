# Task 3

Assuming shell 1 has,

```ruby
class Apple

  attr_reader :variety, :origin, :history
  def initialize(**args)
    @variety = args[:variety]
    @origin = args[:origin]
    @history = args[:history]
  end
end

apple = Apple.new(variety: 'Honeycrisp', origin: 'Minnesota', history: 'Introduction to Market: 1991')
```

and shell 2 has,

```ruby
class Apple

  attr_reader :variety, :origin, :history
  def initialize(**args)
    @variety = args[:variety]
    @origin = args[:origin]
    @history = args[:history]
  end
end
```

Within shell 1 we can do

```ruby
require 'json'

apple_hash = apple.instance_variables.map do |var|
  [var[1..-1], apple.send(var[1..-1])]
end.to_h

File.write('apple.json', apple_hash.to_json)
```

Within shell 2 we can do

```ruby
require 'json'

json = File.read('apple.json')
class Apple
  def []=(key, value)
    instance_variable_set("@#{key}", value)
  end
end
apple = JSON.parse(json, object_class: Apple)
```
