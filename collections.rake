task :add_apple_to_basket, %i[variety count] do |task, args|
  basket_to_modify = Basket.where do |basket|
    basket.apples.length.zero? || basket.apples.any? { |apple| apple.variety == args[:variety] }
  end
  args[:count].times do
    if basket_to_modify.nil?
      puts "All baskets are full. We couldn't find the place for #{args[:count]} apples"
      next
    end
    Apple.create(variety: args[:variety], basket: basket_to_modify)
    basket_to_modify.update(fill_rate: basket_to_modify.apples.size / basket_to_modify.capacity.to_f)
    if basket_to_modify.apples.size == basket_to_modify.capacity
      basket_to_modify = Basket.where do |basket|
        basket.apples.length.zero? || basket.apples.any? { |apple| apple.variety == args[:variety] }
      end
    end
  end
end
