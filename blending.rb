module Blending
  after_save :make_juice_invoke

  def make_juice_invoke
    make_juice if self.class == Apple
  end
end
